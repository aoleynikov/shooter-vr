﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unit9.ShooterVR
{
    public interface IGameController
    {
        void SetupScene(int curLevel);

    }

}
