﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Unit9.ShooterVR
{
    /// <summary>
    /// Entry point of the game
    /// </summary>
    public class GameManager : MonoBehaviour
    {
        [SerializeField]
        private GameConfig _config;

        [SerializeField]
        private UIController _uiController;

        [SerializeField]
        private SoundPlayer _soundPlayer;

        [SerializeField]
        private Transform _playerTransform;

        [SerializeField]
        private int _curLevel = 0;

        private IGameController _gameController;

        private AsyncProcessor _asyncProcessor;

        private void Awake()
        {
            SetupDependencies();
            InitGame();
        }

        private void SetupDependencies()
        {
            // create empty monobehaviour to pass it to regular classes
            // for having access to coroutines
            GameObject asyncProc = new GameObject();
            asyncProc.name = "Async Processor";
            _asyncProcessor = asyncProc.AddComponent<AsyncProcessor>();            

            // create new GameController
            _gameController = new GameController(_config, _asyncProcessor, _uiController, _soundPlayer, _playerTransform);
        }

        private void InitGame()
        {
            // init current level
            _gameController.SetupScene(_curLevel);
        }

    }

}
