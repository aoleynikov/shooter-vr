﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Unit9.ShooterVR
{
    [Serializable]
    public class EnemiesList
    {
        public List<BaseEnemy> enemiesList;
    }

    [CreateAssetMenu(fileName = "WavesConfig", menuName = "Unit9/Waves Config")]
    public class WavesConfig : ScriptableObject
    {
        [Tooltip("Enemies on level")]
        public List<EnemiesList> enemies;
        [Tooltip("Raidus of enemies spawn")]
        public float radius = 10;
        [Tooltip("Enemies speed multiplier")]
        public float speedIncrement = 1;
        [Tooltip("Enemies count on first wave")]
        public int startEnemiesCount = 10;
        [Tooltip("Pause between enemies spawn")]
        public float pauseBetweenEnemies = 2;
        [Tooltip("Pause between waves")]
        public float pauseBetweenWaves = 5;
        [Tooltip("Enemies increment per wave")]
        public int enemiesPerWaveIncrement = 5;
    }
}
