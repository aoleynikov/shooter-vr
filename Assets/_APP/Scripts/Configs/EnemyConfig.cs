﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unit9.ShooterVR
{
    [CreateAssetMenu(fileName = "EnemyConfig", menuName = "Unit9/Enemy Config")]
    public class EnemyConfig : ScriptableObject
    {
        [Tooltip("Player health decrement when enemy hits it")]
        public int playerHitDamage = 20;
        [Tooltip("Enemy's speed")]
        public float speed = 20;
        [Tooltip("Enemy's turn speed when shooting")]
        public float turnSpeed = 10f;
        [Tooltip("Bonus player gets when he kills enemy")]
        public int bonusForKill = 50;

    }
}
