﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unit9.ShooterVR
{
    [CreateAssetMenu(fileName = "GameConfig", menuName = "Unit9/Game Config")]
    public class GameConfig : ScriptableObject
    {
        [Tooltip("Levels in game")]
        public List<LevelConfig> levels;
    }
}