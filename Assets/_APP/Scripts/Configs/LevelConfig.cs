﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unit9.ShooterVR {

    [CreateAssetMenu(fileName = "LevelConfig", menuName = "Unit9/Level Config")]
    public class LevelConfig : ScriptableObject
    { 
        [Tooltip("Name of scene")]
        public string scenePath;

        [Tooltip("Maximum health of player on the level")]
        public int maxPlayerHealth;

        public WavesConfig wavesConfig;
     
    }

}
