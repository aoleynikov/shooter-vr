﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TakeDamage : MonoBehaviour {

    [SerializeField]
    private int _health = 200;

    public UnityAction OnDestroy;

    public UnityAction<float> OnDamage;

    private int _maxHealth;


    [SerializeField]
    private bool _canTakeDamage = false;

    public bool CanTakeDamage { get { return _canTakeDamage; } set { _canTakeDamage = value; } }

    private void Awake()
    {
        _maxHealth = _health;
    }


    public void Damage(int damage)
    {
        if (_canTakeDamage)
        {
            _health -= damage;

            _health = Mathf.Clamp(_health, 0, _maxHealth);

            if (_health == 0)
            {
                if (OnDestroy != null)
                    OnDestroy.Invoke();
            }
            else
            {
                float lifePercent = (float)_health / (float)_maxHealth;

                if (OnDamage != null)
                    OnDamage.Invoke(lifePercent);
            }
        }
    }

}
