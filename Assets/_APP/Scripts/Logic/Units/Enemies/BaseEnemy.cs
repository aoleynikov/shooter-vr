﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Unit9.ShooterVR
{
    public abstract class BaseEnemy : MonoBehaviour
    {
        
        [SerializeField]
        protected IWavesController _wavesController;

        [SerializeField]
        protected UIUnit _enemyUI;

        [SerializeField]
        protected TakeDamage _damager;

        public EnemyConfig enemyConfig;

        [SerializeField]
        protected EventTrigger _eventTrigger;

        protected Transform _target;
        protected WavesConfig _enemiesConfig;
        protected float _speedIncrement = 1;

        public UnityAction<int> OnHitPlayer;
        public UnityAction<BaseEnemy> OnEnemyWayFinished;
        public UnityAction<BaseEnemy> OnEnemyKilled;

        public abstract void DamageEnemy(float lifePercent);
        public abstract void DestroyEnemy();
        public abstract void DestroyEnemyWhenHitToPlayer();


        public virtual void Construct(Transform target, WavesConfig enemiesConfig, IWavesController wavesController, float speedIncrement = 1)
        {
            _wavesController = wavesController;
            _enemiesConfig = enemiesConfig;
            _target = target;

            _speedIncrement = speedIncrement;

            if (_damager == null)
            {
                _damager = GetComponentInChildren<TakeDamage>();
            }

            _damager.OnDamage += DamageEnemy;
            _damager.OnDestroy += DestroyEnemy;


            float angle = Random.Range(0.0f, 2.0f * Mathf.PI);

            float x = _enemiesConfig.radius * Mathf.Cos(angle);
            float y = _enemiesConfig.radius * Mathf.Sin(angle);

            transform.position = new Vector3(_target.position.x + x, 0, _target.position.y + y);
        }
    }
}
