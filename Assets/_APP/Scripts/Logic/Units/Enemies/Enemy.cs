﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unit9.ShooterVR
{
    public class Enemy : BaseEnemy
    {        

        public override void Construct(Transform target, WavesConfig enemiesConfig, IWavesController wavesController, float speedIncrement = 1)
        {
            base.Construct(target, enemiesConfig, wavesController, speedIncrement);          
        }

        private void OnDisable()
        {
            _damager.OnDamage -= DamageEnemy;
            _damager.OnDestroy -= KillEnemy;
        }

        private void Update()
        {
            LockOnTarget(_target);

            // move
            Vector3 dir = _target.position - transform.position;

            transform.Translate(dir.normalized * (enemyConfig.speed + _speedIncrement) * Time.deltaTime, Space.World);


        }

        public void LockOnTarget(Transform target)
        {
            Vector3 dir = transform.position - target.position;
            Quaternion lookRotation = Quaternion.LookRotation(dir);
            Vector3 rotation = Quaternion.Lerp(transform.rotation, lookRotation, Time.deltaTime * enemyConfig.turnSpeed).eulerAngles;
            transform.rotation = Quaternion.Euler(0f, rotation.y, 0f);
        }


        private void OnTriggerEnter(Collider other)
        {
            if (other.tag.Equals("Player"))
            {
                HitPlayer();
            }
        }


        private void HitPlayer()
        {
            if (OnHitPlayer != null)
            {
                OnHitPlayer.Invoke(enemyConfig.playerHitDamage);
            }

           
            DestroyEnemyWhenHitToPlayer();
        }

        public override void DamageEnemy(float lifePercent)
        {
            _enemyUI.SetLifePercent(lifePercent);
        }

        public override void DestroyEnemy()
        {
            OnEnemyKilled.Invoke(this);
            KillEnemy();
        }

        public override void DestroyEnemyWhenHitToPlayer()
        {
            OnEnemyWayFinished.Invoke(this);
            KillEnemy();                      
        }

        private void KillEnemy()
        {
            

            if (gameObject != null)
                Destroy(gameObject);
        }
    }
}
