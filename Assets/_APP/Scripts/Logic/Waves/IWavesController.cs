﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unit9.ShooterVR
{
    public interface IWavesController
    {
        void SpawnWaves();

        BaseEnemy SpawnEnemy(BaseEnemy enemy);
    }
}