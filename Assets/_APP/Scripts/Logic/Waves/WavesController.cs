﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Unit9.ShooterVR
{
    public class WavesController : IWavesController
    {
        private WavesConfig _wavesConfig;
        private AsyncProcessor _asyncProcessor;
        private Coroutine _waveCoroutine;

        private List<BaseEnemy> _activeEnemies = new List<BaseEnemy>();

        private Transform _target;

        public UnityAction<int> OnHitPlayer;
        public UnityAction<BaseEnemy> OnKillEnemy;
        public UnityAction<int> OnNewWave;

        private bool _playing = false;
        private int _waveId = 0;
        private float _speedIncrement = 1;

        public WavesController( WavesConfig enemiesConfig,
                                AsyncProcessor asyncProcessor,
                                Transform target) 
        {
            _wavesConfig = enemiesConfig;
            _target = target;
            _asyncProcessor = asyncProcessor;
        }

        public void SpawnWaves()
        {
            _playing = true;
            _waveId = 0;
            _waveCoroutine = _asyncProcessor.StartCoroutine(SpawnWavesCoroutine());
        }

        public void StopWaves()
        {
            _playing = false;
        }

        private IEnumerator SpawnWavesCoroutine()
        {
            while (_playing)
            {
                OnNewWave.Invoke(_waveId);

                int enemiesIncrement = _waveId * _wavesConfig.enemiesPerWaveIncrement;
                _speedIncrement = _waveId * _wavesConfig.speedIncrement;

                for (int i = 0; i < _wavesConfig.startEnemiesCount + enemiesIncrement; i++)
                {
                    int enemiesList = _waveId < _wavesConfig.enemies.Count ? _waveId : _wavesConfig.enemies.Count - 1;

                    int enemyIndex = UnityEngine.Random.Range(0, _wavesConfig.enemies[enemiesList].enemiesList.Count);                  

                    SpawnEnemy(_wavesConfig.enemies[enemiesList].enemiesList[enemyIndex]);
                    yield return new WaitForSeconds(_wavesConfig.pauseBetweenEnemies);
                }

                _waveId++;
                yield return new WaitForSeconds(_wavesConfig.pauseBetweenWaves);
                
            }
        }

        public BaseEnemy SpawnEnemy(BaseEnemy enemy)
        {
            BaseEnemy spawnedEnemy = GameObject.Instantiate(enemy.gameObject).GetComponent<BaseEnemy>();

            spawnedEnemy.Construct(_target, _wavesConfig, this, _speedIncrement);

            spawnedEnemy.OnHitPlayer += HitPlayer;
            spawnedEnemy.OnEnemyKilled += EnemyKilled;
            spawnedEnemy.OnEnemyWayFinished += RemoveEnemy;

            _activeEnemies.Add(spawnedEnemy);

            return spawnedEnemy;
        }

        public void HitPlayer(int damage)
        {
            OnHitPlayer.Invoke(damage);
        }

        public void Stop()
        {
            _asyncProcessor.StopCoroutine(_waveCoroutine);

            RemoveAllEnemies();
        }

        private void EnemyKilled(BaseEnemy enemy)
        {
            OnKillEnemy.Invoke(enemy);
            RemoveEnemy(enemy);
        }

        public void RemoveEnemy(BaseEnemy enemy)
        {
            // remove passed enemy from the list
            for (int i = _activeEnemies.Count - 1; i >= 0; i--)
            {
                if (_activeEnemies[i] != null && _activeEnemies[i] == enemy)
                {
                    _activeEnemies.RemoveAt(i);
                }
                else if (_activeEnemies[i] == null)
                {
                    _activeEnemies.RemoveAt(i);
                }
            }


        }

        public void RemoveAllEnemies()
        {
            for (int i = _activeEnemies.Count - 1; i >= 0; i--)
            {
                try
                {
                    _activeEnemies[i].DestroyEnemyWhenHitToPlayer();
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                }
            }

            _activeEnemies.Clear();

        }

 
    }

}
