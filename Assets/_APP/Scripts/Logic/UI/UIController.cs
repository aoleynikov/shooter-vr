﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Unit9.ShooterVR
{
    public class UIController : MonoBehaviour
    {
        [SerializeField]
        private GameObject _startButton;

        [SerializeField]
        private Text _currentPlayerHealth;

        [SerializeField]
        private Text _currentScore;

        [SerializeField]
        private Text _finalScore;

        [SerializeField]
        private Text _currentLevel;

        [SerializeField]
        private Text _currentWave;

        [SerializeField]
        private GameObject _finalScreen;



        [SerializeField]
        private GameObject _statsPanel;

        public UnityAction OnReplay;
        public UnityAction OnNextLevel;
        public UnityAction OnPlayGame;
        

        public void Setup()
        {
            _startButton.SetActive(true);

            _finalScreen.SetActive(false);

            _statsPanel.SetActive(false);
        }

        public void PlayGame()
        {
            OnPlayGame.Invoke();
            _startButton.SetActive(false);
            _statsPanel.SetActive(true);
        }
               

        public void SetPlayerHealth(int playerHealth)
        {
            _currentPlayerHealth.text = "" + playerHealth;
        }

        public void SetCurrentScore(int score)
        {
            _currentScore.text = _finalScore.text = "" + score;
        }


        public void LevelFinished()
        {
            _finalScreen.SetActive(true);
        }

        public void SetCurrentLevel(int level)
        {
            _currentLevel.text = "" + level;
        }

        public void Replay()
        {
            OnReplay.Invoke();
        }

        public void Next()
        {
            OnNextLevel.Invoke();
        }

        public void ShowWave(int waveId)
        {
            _currentWave.text = "" + (waveId + 1);
        }
    }
}
