﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Unit9.ShooterVR
{
    public class UIUnit : MonoBehaviour
    {
        [SerializeField]
        private Image _life;

        public void SetLifePercent(float lifePercent)
        {
            _life.fillAmount = lifePercent;
        }

    }
}