﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Unit9.ShooterVR
{
    /// <summary>
    /// The heart of game logic
    /// </summary>
    public class GameController : IGameController
    {
        // game config — scriptable object
        private GameConfig _gameConfig;
        // async processor for access to coroutines
        private AsyncProcessor _asyncProcessor;

        // waves controller
        private WavesController _wavesController;

        // ui controller passed from outside (from game manager)
        private UIController _uiController;

        // sounds controller
        private SoundPlayer _soundPlayer;

        // level to run
        private int _curLevel = 0;
        // current base health
        private int _playerHealth;
        // maximum base health
        private int _maxPlayerHealth;
        // current score
        private int _curScore = 0;
        // player transform
        private Transform _playerTransform;
        
        public GameController(  GameConfig gameConfig, 
                                AsyncProcessor asyncProcessor, 
                                UIController uiController, 
                                SoundPlayer soundPlayer,
                                Transform playerTransform)
        {
            _gameConfig = gameConfig;
            _asyncProcessor = asyncProcessor;
            _uiController = uiController;
            _soundPlayer = soundPlayer;
            _playerTransform = playerTransform;
        }

        public void SetupScene(int curLevel)
        {
            _curLevel = curLevel;
            _maxPlayerHealth = _gameConfig.levels[_curLevel].maxPlayerHealth;
            _playerHealth = _maxPlayerHealth;
            
            // setup UI controller
            _uiController.Setup();
            _uiController.OnPlayGame += PlayGame;

            _uiController.SetCurrentLevel(_curLevel + 1);
            _uiController.SetPlayerHealth(_maxPlayerHealth);

            _uiController.OnNextLevel += RunNextLevel;
            _uiController.OnReplay += ReplayLevel;
            

            // setup waves controller
            _wavesController = new WavesController(_gameConfig.levels[_curLevel].wavesConfig,
                                                   _asyncProcessor,
                                                   _playerTransform.gameObject.transform);

            _wavesController.OnHitPlayer += HitPlayer;
            _wavesController.OnKillEnemy += EnemyKilled;
            _wavesController.OnNewWave += NewWave;
        }

        /// <summary>
        /// Called on new wave
        /// </summary>
        /// <param name="waveId"></param>
        private void NewWave(int waveId)
        {
            _uiController.ShowWave(waveId);
        }

        /// <summary>
        /// Called when play game called in UI
        /// </summary>
        private void PlayGame()
        {
            // Start game
            _wavesController.SpawnWaves();
            _soundPlayer.Play(true);
        }


        /// <summary>
        /// Called when enemy killed
        /// </summary>
        /// <param name="enemy"></param>
        private void EnemyKilled(BaseEnemy enemy)
        {
            _curScore += enemy.enemyConfig.bonusForKill;
            _uiController.SetCurrentScore(_curScore);
        }

        /// <summary>
        /// Calls when enemy hits bese
        /// </summary>
        /// <param name="damage"></param>
        private void HitPlayer(int damage)
        {
            _playerHealth -= damage;
            _playerHealth = Mathf.Clamp(_playerHealth, 0, _maxPlayerHealth);

            _uiController.SetPlayerHealth(_playerHealth);

            if (_playerHealth == 0)
            {
                _wavesController.Stop();

                _uiController.LevelFinished();

                _soundPlayer.Stop();

            }           
        }

        /// <summary>
        /// If we want to replay level
        /// </summary>
        public void ReplayLevel()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        /// <summary>
        /// If we want to run next level
        /// </summary>
        public void RunNextLevel()
        {
            if (_curLevel + 1 < _gameConfig.levels.Count)
            {
                SceneManager.LoadScene(_gameConfig.levels[_curLevel + 1].scenePath);
            }
        }


    }
}
